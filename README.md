## Prerequisites
* AWS CLI - https://aws.amazon.com/cli
* AWS CDK - https://aws.amazon.com/cdk/
* Python3 - https://www.python.org
* Node - https://nodejs.org/en/

Some of these tools can be installed using brew for mac.
```
brew install awscli
brew install node
brew install python
```

The AWS CDK is installed using NPM
```
npm install -g aws-cdk
```

Create a directory and pull this repo
```
mkdir aws-cdk-sandbox && cd aws-cdk-sandbox
```

Create a virtual environment .env
```
python3 -m venv .env
source .env/bin/activate
```

Once you've got your virtualenv running, run the following command to install the required dependencies
```
pip install -r requirements.txt
```

## Setting the AWS API key

Once the awscli tool is installed run:
```
aws configure
```
it will ask for the following
* AWS Access Key ID - [your AWS Access Key ID]
* AWS Secret Access Key - [your accounts Secret Access Key]
* Default region name - [your local region]
* Default output format - [json]

The AWS CDK will now use the Access Key ID and Secret you set to create resources in your account.