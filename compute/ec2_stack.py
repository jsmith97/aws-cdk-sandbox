from aws_cdk import (
    core,
    aws_ec2 as ec2
)
# import if you need to search for the latest version of an AMI in your account

# from compute.ami_search import find_latest_ami

# props in the function def below imports the
# ec2.vpc from the network/vpc_stack.py


class ec2_stack(core.Stack):
    def __init__(self, scope: core.Construct, id: str, props: ec2.Vpc,
                 **kwargs) -> None:
        super().__init__(scope, id, **kwargs)

        # Select the latest Amazon Linux 2 AMI - if using a public AWS AMI

        aws_linux = ec2.AmazonLinuxImage(
             generation=ec2.AmazonLinuxGeneration.AMAZON_LINUX_2,
             edition=ec2.AmazonLinuxEdition.STANDARD,
             virtualization=ec2.AmazonLinuxVirt.HVM,
             storage=ec2.AmazonLinuxStorage.GENERAL_PURPOSE,
        )

        # Select the latest version of an ami - if using a private AMI

        # london_bastion_ami_id = find_latest_ami(
        #     'self',
        #     'eu-west-2',
        #     'bastion-server',
        #     'x86_64',
        #     'ebs'
        #     )
        # bastion_image = ec2.GenericLinuxImage({
        #     'eu-west-2': london_bastion_ami_id,
        # })

        # Create security group to allow SSH access from anywhere

        bastion_sg = ec2.SecurityGroup(
            self, "bastion-sg",
            vpc=props,
            allow_all_outbound=True,
        )
        bastion_sg.add_ingress_rule(
            peer=ec2.Peer.any_ipv4(),
            connection=ec2.Port.tcp(22),
            description='allow ssh from anywhere to bastion server'
        )

        # create a bastion server instance on EC2

        bastion_sandbox = ec2.Instance(
            self, "bastion",
            instance_type=ec2.InstanceType('t3.micro'),
            machine_image=aws_linux,
            security_group=bastion_sg,
            vpc_subnets=ec2.SubnetSelection(
                subnet_type=ec2.SubnetType.PUBLIC
            ),
            vpc=props,
            key_name='sandbox-key'
        )

        # Tag all of the resources that you have createds

        core.Tag.add(bastion_sandbox, "stack", "compute")
        core.Tag.add(bastion_sandbox, "env", "test")
        core.Tag.add(bastion_sg, "stack", "compute")
        core.Tag.add(bastion_sg, "env", "test")
