import boto3


def find_latest_ami(owner, aws_region, name, arch, disk_type):
    EC2 = boto3.client('ec2', region_name=aws_region)
    response = EC2.describe_images(
        Owners=[owner],
        Filters=[
          {'Name': 'name', 'Values': [name]},
          {'Name': 'architecture', 'Values': [arch]},
          {'Name': 'root-device-type', 'Values': [disk_type]},
        ],
    )
    amis = sorted(response['Images'],
                  key=lambda x: x['CreationDate'],
                  reverse=True)
    return(amis[0]['ImageId'])
