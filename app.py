from aws_cdk import core

# import all the stacks you have defined

from network.vpc_stack import vpc_stack
from compute.ec2_stack import ec2_stack

app = core.App()

# define the network stack

network_vpc = vpc_stack(app, "vpc-sandbox", env={'region': 'eu-west-2'})

# define the compute stack and pass the sandbox_vpc object to it

compute_ec2 = ec2_stack(app, "ec2-sandbox", network_vpc.sandbox_vpc,
                        env={'region': 'eu-west-2'})

# create a dependency between the compute and network stacks

compute_ec2.add_dependency(network_vpc)

app.synth()
