from aws_cdk import (
    core,
    aws_ec2 as ec2
)
import os


class vpc_stack(core.Stack):
    def __init__(self, scope: core.Construct, id: str, **kwargs) -> None:
        super().__init__(scope, id, **kwargs)

        # Define an AWS VPC
        # VPC cidr and max AZs is taken from the cdk.json file using context

        vpc_cidr = self.node.try_get_context('cidr_by_env')[os.getenv(
            'environment', 'dev'
            )]
        my_max_azs = self.node.try_get_context('max_azs')[os.getenv(
            'environment', 'dev'
        )]
        self.sandbox_vpc = ec2.Vpc(
            self, 'vpc',
            cidr=vpc_cidr,
            max_azs=my_max_azs,
            nat_gateways=1,
            subnet_configuration=[
                ec2.SubnetConfiguration(
                    name="public", cidr_mask=24,
                    subnet_type=ec2.SubnetType.PUBLIC),
                ec2.SubnetConfiguration(
                    name="private", cidr_mask=24,
                    subnet_type=ec2.SubnetType.PRIVATE)
                    ]
            )

        # Tag the resources you created

        core.Tag.add(self.sandbox_vpc, "stack", "network")
        core.Tag.add(self.sandbox_vpc, "env", "test")
